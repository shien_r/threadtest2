import javax.swing.JTextArea;
import javax.swing.SwingWorker;


public class PrimeWorker extends SwingWorker<Boolean, Object>{

	int value = 0;
	JTextArea jTextArea;
	
	PrimeWorker(int _value, JTextArea _jTextArea) {
		value = _value;
		jTextArea = _jTextArea;
	}
	
	@Override
	protected Boolean doInBackground() throws Exception {
		boolean flag = true;
		//for (int i = 2; i <= Math.sqrt(value); i++) {
		for (int i = 2; i <= Math.sqrt(value); i++) { 
			for (int j=2; j<=Math.sqrt(value); j++) {// 時間をかけたいだけの意味のない行為
				if (value%i==0) {
					flag = false;
				}
			}
		}
		synchronized(jTextArea) {
			if (flag) {
				jTextArea.insert(value + "は素数だよ\n", 0);
			} else {
				jTextArea.insert(value + "は素数じゃない\n", 0);
			}
			jTextArea.insert(Thread.currentThread().toString(), 0);		
		}
		return true;
	}
	
	
}
