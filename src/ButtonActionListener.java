import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextArea;
import javax.swing.SwingWorker;

public class ButtonActionListener implements ActionListener {

	JTextArea jTextArea;
	int count = 1_000_000_000;
	ArrayList<PrimeWorker> primeWorkers;
	final int MAX = 5;
	
	ButtonActionListener(JTextArea _jTextArea) {
		jTextArea = _jTextArea;
		primeWorkers = new ArrayList<PrimeWorker>();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		createThread();
		count++;
	}
	
	private void createThread() { // 起動しているスレッドの数を５個に制限
		int size = primeWorkers.size();
		if (size == 0) { // ArrayList に要素がひとつもないとき。初期化みたいなの
			primeWorkers.add(executePrimeWorker());
			return;
		}
		for (int i=0; i<MAX && i < size;i++) { // 要素をすべて見て、完了したスレッドを探す
			PrimeWorker primeWorker = primeWorkers.get(i);
			SwingWorker.StateValue state = primeWorker.getState();
			if(SwingWorker.StateValue.DONE==state) {
				primeWorkers.set(i,executePrimeWorker());
				System.out.println("DONE "+ "thread number " + i); 
				return;
			}
		}
		if (size <= MAX) { // 完了したスレッドがなく、要素が５以下なら
			primeWorkers.add(executePrimeWorker());
			return;
		}
		System.out.println("スレッド作れないよ！！");
		return;
		
	}
	
	private PrimeWorker executePrimeWorker() {
		PrimeWorker newPrimeWorker = new PrimeWorker(count, jTextArea);
		newPrimeWorker.execute();
		return newPrimeWorker;
	}

}
