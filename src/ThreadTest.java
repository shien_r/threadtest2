import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class ThreadTest extends JFrame implements Runnable {

	static Integer foo = 0;
	
	ThreadTest () {
		this.setSize(500,700);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new FlowLayout());
		JTextArea jTextArea = new JTextArea();
		jTextArea.setRows(80);
		jTextArea.setColumns(40);
		JScrollPane jScrollPane = new JScrollPane(jTextArea);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JButton jButton = new JButton("start !!");
		jButton.addActionListener(new ButtonActionListener(jTextArea));
		this.add(jButton);
		this.add(jScrollPane);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new ThreadTest());
	}

	@Override
	public void run() {
		this.setVisible(true);
	}
}
